package ai.maum.aiHuman.controller;

import ai.maum.aiHuman.data.MvpResponse;
import ai.maum.aiHuman.data.SdsResponseData;
import ai.maum.aiHuman.service.AiHumanDemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AiHumanDemoController {

    final
    AiHumanDemoService service;
    final Logger logger = LoggerFactory.getLogger(this.getClass());

    public AiHumanDemoController(AiHumanDemoService service) {
        this.service = service;
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/callAiHuman")
    @ResponseBody
    public ResponseEntity<MvpResponse> callAiHuman(
            @RequestParam(value = "input_text", required = false) String inputText,
            @RequestParam(value = "input_file",required = false) MultipartFile inputFile,
            @RequestParam(value = "input_type") String inputType
    ) {
        logger.info(inputType);
        MvpResponse result;

        if (inputType.equals("text")) {
            logger.info(inputText);
            result = service.startFromSds(inputText);
        } else {
            logger.info(inputFile.getOriginalFilename());
            result = service.startFromStt(inputFile);
        }

        return ResponseEntity.ok(result);
    }
}
