package ai.maum.aiHuman.service;

import ai.maum.aiHuman.client.DemoHttpClient;
import ai.maum.aiHuman.data.MvpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class AiHumanDemoService {
    final
    DemoHttpClient demoHttpClient;

    public AiHumanDemoService(DemoHttpClient demoHttpClient) {
        this.demoHttpClient = demoHttpClient;
    }

    public MvpResponse startFromStt(MultipartFile inputFile) {
        String sttReuslt = demoHttpClient.sttCall(inputFile);
        if (sttReuslt == null) return null;
        return startFromSds(sttReuslt);
    }

    public MvpResponse startFromSds(String inputText) {

        String sdsResult = demoHttpClient.sdsCall(inputText);
        if (sdsResult == null) return null;
        return new MvpResponse(sdsResult, demoHttpClient.avatarCall(sdsResult));
    }
}
