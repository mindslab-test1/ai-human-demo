package ai.maum.aiHuman.data;

import lombok.Data;

@Data
public class TestRequestData {
    String apiId;
    String apiKey;
    String context;

    public TestRequestData(String apiId, String apiKey, String context) {
        this.apiId = apiId;
        this.apiKey = apiKey;
        this.context = context;
    }
}
