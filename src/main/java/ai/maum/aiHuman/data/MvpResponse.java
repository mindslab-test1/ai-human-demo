package ai.maum.aiHuman.data;

import lombok.Data;

@Data
public class MvpResponse {
    private String avatarKey;
    private String sdsResponse;

    public MvpResponse(String avatarKey, String sdsResponse) {
        this.avatarKey = avatarKey;
        this.sdsResponse = sdsResponse;
    }
}
