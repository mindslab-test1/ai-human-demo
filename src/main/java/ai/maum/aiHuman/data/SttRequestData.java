package ai.maum.aiHuman.data;

import lombok.Data;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Data
public class SttRequestData {

    private String apiId;
    private String apiKey;
    private MultipartFile file;
    private String model;

    public SttRequestData(MultipartFile file) {
        this.apiId = "curogom-mindslab";
        this.apiKey = "96395eb75d3943cbadd28c8da50292ab";
        this.file = file;
        this.model = "ncocomix_kor_16k";
    }

    public MultiValueMap<String, Object> getBody() throws IOException {
        LinkedMultiValueMap body = new LinkedMultiValueMap();
        body.add("apiId", this.apiId);
        body.add("apiKey", this.apiKey);
        body.add("file", new ByteArrayResource(this.file.getBytes()));
        body.add("model", this.model);

        return body;
    }
}
