package ai.maum.aiHuman.data;

import lombok.Data;

@Data
public class SdsRequestData {
    private String host;
    private String session;
    private SdsData data;
    private String lang;

    public SdsRequestData(String host, String session, String utter, String lang) {
        this.host = host;
        this.session = session;
        this.data = new SdsData(utter);
        this.lang = lang;
    }
}

@Data
class SdsData {
    private String utter;

    public SdsData(String utter) {
        this.utter = utter;
    }
}
