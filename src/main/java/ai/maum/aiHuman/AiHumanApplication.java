package ai.maum.aiHuman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class AiHumanApplication {

    public static void main(String[] args) {
        SpringApplication.run(AiHumanApplication.class, args);
    }

}
