package ai.maum.aiHuman.client;

import ai.maum.aiHuman.data.SdsRequestData;
import ai.maum.aiHuman.data.SdsResponseData;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Component
public class DemoHttpClient {
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    final Gson gson = new Gson();

    @Value("${mindslab.api.stt.url}")
    private String sttUrl;

    @Value("${mindslab.api.stt.model}")
    private String sttModel;

    @Value("${mindslab.api.sds.url}")
    private String sdsUrl;

    @Value("${mindslab.api.sds.host}")
    private String sdsHost;

    @Value("${mindslab.api.sds.session}")
    private String sdsSession;

    @Value("${mindslab.api.sds.lang}")
    private String sdsLang;

    @Value("${mindslab.api.avatar.url}")
    private String avatarUrl;

    @Value("${mindslab.api.apiId}")
    private String apiId;

    @Value("${mindslab.api.apiKey}")
    private String apiKey;

    private final RestTemplate restTemplate;

    public DemoHttpClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String sttCall(MultipartFile inputFile) {
        HttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(sttUrl);
        File file = null;
        String result = null;

        try {
            file = convertFile(inputFile, "STT_", ".wav");

            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
            multipartEntityBuilder.addPart("file", new FileBody(file));
            multipartEntityBuilder.addTextBody("apiId", apiId);
            multipartEntityBuilder.addTextBody("apiKey", apiKey);
            multipartEntityBuilder.addTextBody("model", sttModel);

            HttpEntity entity = multipartEntityBuilder.build();

            httpPost.setEntity(entity);
            HttpResponse response = client.execute(httpPost);
            Map responseBody = parseResponse(response);

            logger.info(String.valueOf(responseBody.get("result")));
            result = String.valueOf(responseBody.get("result"));

        } catch (IOException e) {
            logger.error(e.toString(), e);
        } finally {
            if (file != null) {
                file.delete();
            }
        }

        return result;
    }

    public String sdsCall(String inputText) {
        SdsRequestData requestData = new SdsRequestData(sdsHost, sdsSession, inputText, sdsLang);
        SdsResponseData response = restTemplate.postForObject(sdsUrl, requestData, SdsResponseData.class);
        String result = String.valueOf(response.getAnswer().get("answer"));
        logger.info(result);

        return result;
    }

    public String avatarCall(String inputText) {
        HttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(avatarUrl);

        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create().setCharset(StandardCharsets.UTF_8);
        multipartEntityBuilder.addTextBody("apiId", apiId);
        multipartEntityBuilder.addTextBody("apiKey", apiKey);
//        multipartEntityBuilder.addTextBody("resolution", "SD");
        multipartEntityBuilder.addTextBody("text", inputText, ContentType.create("Multipart/related", "UTF-8"));

        String result = null;

        try {
            HttpEntity entity = multipartEntityBuilder.build();

            httpPost.setEntity(entity);
            HttpResponse response = client.execute(httpPost);
            Map responseBody = parseResponse(response);
            result = String.valueOf(((Map) responseBody.get("payload")).get("requestKey"));

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    private Map parseResponse(HttpResponse response) throws IOException {
        int responseCode = response.getStatusLine().getStatusCode();
        if ((responseCode/100) != 2) {
            logger.warn(String.valueOf(responseCode));
            return null;
        }

        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(response.getEntity().getContent())));

        StringBuilder stringBuilder = new StringBuilder();
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }

        return gson.fromJson(stringBuilder.toString(), HashMap.class);
    }

    private File convertFile(MultipartFile multipartFile, String prefix, String suffix) throws IOException{
        Path tempPath = Files.createTempFile(prefix, suffix);
        Files.write(tempPath, multipartFile.getBytes());
        return tempPath.toFile();
    }
}
